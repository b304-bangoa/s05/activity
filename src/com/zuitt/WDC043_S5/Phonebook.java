package com.zuitt.WDC043_S5;

import java.util.ArrayList;

public class Phonebook {

    //Arraylist of Contact objects
    private ArrayList<Contact> contacts;

    //Constructors
    public Phonebook() {
        contacts = new ArrayList<Contact>();
    }
    public Phonebook(ArrayList<Contact> contacts) {
        this.contacts = contacts;
    }

    //Getter & Setter
    public ArrayList<Contact> getContacts() {
        return this.contacts;
    }

    public void setContacts(ArrayList<Contact> contacts) {
        this.contacts = contacts;
    }

    //Add contact method
    public void addContact(Contact contact) {
        contacts.add(contact);
    }

}
