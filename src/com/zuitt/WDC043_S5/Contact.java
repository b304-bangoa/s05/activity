package com.zuitt.WDC043_S5;

public class Contact {

    private String name;
    private String contactNumber;
    private String address;

    //Constructors
    public Contact() {}

    public Contact(String name, String contactNumber1, String contactNumber2, String address1, String address2) {
        this.name = name;
        this.contactNumber = contactNumber1 + "\n" + contactNumber2;
        this.address = address1 + "\n" +address2;
    }

    //Getter & Setter


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

}
