import com.zuitt.WDC043_S5.Contact;
import com.zuitt.WDC043_S5.Phonebook;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {

        Phonebook phonebook = new Phonebook();

       /* Contact contact1 = new Contact("John Doe", "+639152468596", "+639228547963", "my home in Quezon City", "my office in Makati City");
        phonebook.addContact(contact1);

        Contact contact2 = new Contact("Jane Doe", "+639162148573", "+639173698541", "my home in Caloocan City", "my office in Pasay City");
        phonebook.addContact(contact2);*/

        //Get list of contacts in phonebook
        ArrayList<Contact> contacts = phonebook.getContacts();

        //Control Structure for empty phonebook
        if (contacts.isEmpty()) {
            System.out.println("The phonebook is empty.");
        } else {

            for (Contact contact : contacts) {
                System.out.println("--------------------");
                System.out.println(contact.getName());
                System.out.println("--------------------");
                System.out.println(contact.getName() + " has the following registered numbers:");
                System.out.println(contact.getContactNumber());
                System.out.println("--------------------");
                System.out.println(contact.getName() + " has the following registered addresses:");
                System.out.println(contact.getAddress());
                System.out.println("===================================");
            }

        }

    }
}